#ifndef COMMONEXAMPLEWIDGET_H
#define COMMONEXAMPLEWIDGET_H

#include <QWidget>

#include "settings.h"

class QLCDNumber;
class QLabel;
class ExampleInfo;

class CommonExampleWidget : public QWidget
{
    Q_OBJECT
public:
    explicit CommonExampleWidget(QWidget *parent = nullptr);

    virtual void genarateValues(const Settings &settings) = 0;
    virtual void genarateValues();

    static int randomValue(int minV, int maxV, bool includedMax = true);
    static int randomValue(int maxV, bool includedMax = true);


public slots:

    virtual void checkResult();

signals:

    void requestUpdate();

protected:

    virtual void keyPressEvent(QKeyEvent *e) override;

    void setupLCD(QLCDNumber *lcd);
    void setupLbl(QLabel *lbl);

    void updateValues(ExampleInfo &ex,
                      const Settings &settings);

    Settings m_currentSettings;

signals:

};

#endif // COMMONEXAMPLEWIDGET_H
