#include "examplewidget.h"

#ifdef QT_DEBUG
#include <QDebug>
#endif



#include "settings.h"

#include "inequalitywidget.h"
#include "samplewidget.h"

ExampleWidget::ExampleWidget(QWidget *parent)
    :QStackedWidget(parent)
{

    m_ineqW = nullptr;
    auto sampW = new SampleWidget(this);

    addWidget(sampW);

}

void ExampleWidget::updateSettings(const Settings &settings)
{
    m_currentSettings = settings;

    if(m_currentSettings.inequality){
        m_ineqW= new InequalityWidget(this);
        addWidget(m_ineqW);
    }

    generate();
}

void ExampleWidget::generate()
{
    auto randIndexWidget = CommonExampleWidget::randomValue(count(), false);

    setCurrentIndex(randIndexWidget);

    if(auto curW = qobject_cast<CommonExampleWidget*>(currentWidget())){

        curW->genarateValues(m_currentSettings);

        connect(curW, &CommonExampleWidget::requestUpdate,[this]{
            check();
        });
    }
}

void ExampleWidget::check()
{
    if(auto curW = qobject_cast<CommonExampleWidget*>(currentWidget())){

        curW->checkResult();

        generate();
    }

}
