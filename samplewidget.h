#ifndef SAMPLEWIDGET_H
#define SAMPLEWIDGET_H

#include "commonexamplewidget.h"

class QLCDNumber;
class QLabel;


class SampleWidget : public CommonExampleWidget
{
    Q_OBJECT
public:
    explicit SampleWidget(QWidget *parent = nullptr);

    virtual void genarateValues(const Settings &settings) override;

signals:

protected slots:

    virtual void checkResult() override;

protected:

    virtual void keyPressEvent(QKeyEvent *e) override;

private:

    QLCDNumber *m_first;
    QLCDNumber *m_second;
    QLCDNumber *m_result;
    QLabel *m_signLbl;

};

#endif // SAMPLEWIDGET_H
