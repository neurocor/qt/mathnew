#ifndef WIDGET_H
#define WIDGET_H

#include <QStackedWidget>

class Widget : public QStackedWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
};
#endif // WIDGET_H
