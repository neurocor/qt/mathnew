#ifndef INEQUALITYWIDGET_H
#define INEQUALITYWIDGET_H

#include "commonexamplewidget.h"

#include <QList>

#include "inequalityinfo.h"

class QLCDNumber;
class QLabel;

class InequalityWidget : public CommonExampleWidget
{
    Q_OBJECT
public:
    explicit InequalityWidget(QWidget *parent = nullptr);

    virtual void genarateValues(const Settings &settings) override;

    QList <InequalityInfo> result()const;

signals:

protected slots:

    virtual void checkResult() override;

protected:

    virtual void keyPressEvent(QKeyEvent *e) override;

private:



    QLCDNumber *m_lFirst;
    QLCDNumber *m_lSecond;
    QLabel *m_lSignLbl;

    QLCDNumber *m_rFirst;
    QLCDNumber *m_rSecond;
    QLabel *m_rSignLbl;

    QLabel *m_ineqSignLbl;

    InequalityInfo curInequality;

    QList <InequalityInfo> m_inequalities;

    //    Example lEx;
    //    Example rEx;

};

#endif // INEQUALITYWIDGET_H
