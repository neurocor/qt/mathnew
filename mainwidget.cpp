#include "mainwidget.h"

#include <QLayout>
#include <QPushButton>

#include "examplewidget.h"

MainWidget::MainWidget(QWidget *parent)
    :QWidget(parent)
{
    auto mainLay = new QVBoxLayout(this);

    m_exW = new ExampleWidget(this);
    auto checkBtn = new QPushButton("Проверить", this);


    mainLay->addWidget(m_exW);
    mainLay->addWidget(checkBtn);


    connect(checkBtn, &QPushButton::clicked,[this]{
        m_exW->check();
    });
}

void MainWidget::setSettings(const Settings &settings)
{
    m_exW->updateSettings(settings);
}
