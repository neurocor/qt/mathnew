#include "commonexamplewidget.h"

#include <QKeyEvent>

#include <QRandomGenerator64>
#include <QLCDNumber>
#include <QLabel>
#include <QFont>

#include "settings.h"
#include "exampleinfo.h"

CommonExampleWidget::CommonExampleWidget(QWidget *parent)
    : QWidget{parent}
{
    setFocusPolicy(Qt::FocusPolicy::StrongFocus);
}

void CommonExampleWidget::genarateValues()
{
    genarateValues(m_currentSettings);
}

int CommonExampleWidget::randomValue(int minV, int maxV, bool includedMax)
{
    int res = 0;

    for(int i = 0; i < 5; ++i)
        res=QRandomGenerator64::global()->bounded(minV, maxV + includedMax);

    return res;
}

int CommonExampleWidget::randomValue(int maxV, bool includedMax)
{
    return randomValue(0, maxV, includedMax);
}

void CommonExampleWidget::checkResult()
{
    emit requestUpdate();
}

void CommonExampleWidget::keyPressEvent(QKeyEvent *e)
{
    if(e->key() == Qt::Key_Enter || e->key() == Qt::Key_Return )
        checkResult();

    QWidget::keyPressEvent(e);
}

void CommonExampleWidget::setupLCD(QLCDNumber *lcd)
{
    lcd->setDigitCount(2);
}

void CommonExampleWidget::setupLbl(QLabel *lbl)
{
    auto tmpFont = lbl->font();
    tmpFont.setPixelSize(72);
    tmpFont.setBold(true);
    lbl->setFont(tmpFont);

    lbl->setAlignment(Qt::AlignCenter);
}


void CommonExampleWidget::updateValues(ExampleInfo &ex, const Settings &settings)
{
    auto st = settings;

    auto randSignIndex = randomValue(st.signs.size(), false);

    ex.sign = st.signs.at(randSignIndex);

    int fVal=0;
    int sVal=0;
    int rVal=0;

    switch (ex.sign) {
    case '+':
    case '-':
        do{
            fVal = randomValue(st.minVal, st.maxVal);
            sVal = randomValue(st.minVal, st.maxVal);
            rVal = fVal + sVal;
        }while(rVal > st.resVal);
        break;
    case '*':
    case ':':
        fVal = randomValue(1, 9);
        sVal = randomValue(1, 9);
        rVal = fVal * sVal;
        break;
    default:
        break;
    }

    switch (ex.sign) {
    case '+':
    case '*':
        ex.fVal = fVal;
        ex.sVal = sVal;
        ex.rVal = rVal;
        break;
    case '-':
    case ':':
        ex.fVal = rVal;
        ex.sVal = sVal;
        ex.rVal = fVal;
        break;
    default:
        break;
    }
}
