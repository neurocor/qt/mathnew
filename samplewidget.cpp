#include "samplewidget.h"

#include <QKeyEvent>

#include <QLayout>
#include <QLabel>
#include <QLCDNumber>

#include "settings.h"
#include "sampleinfo.h"


SampleWidget::SampleWidget(QWidget *parent)
    : CommonExampleWidget{parent}
{
    auto mainLay=new QHBoxLayout(this);

    m_first= new QLCDNumber(this);
    m_second= new QLCDNumber(this);
    m_result= new QLCDNumber(this);

    m_signLbl=new QLabel(this);
    auto eqLbl=new QLabel("=", this);

    mainLay->addWidget(m_first);
    mainLay->addWidget(m_signLbl);
    mainLay->addWidget(m_second);
    mainLay->addWidget(eqLbl);
    mainLay->addWidget(m_result);

    setupLCD(m_first);
    setupLCD(m_second);
    setupLCD(m_result);

    setupLbl(m_signLbl);
    setupLbl(eqLbl);
}

void SampleWidget::genarateValues(const Settings &settings)
{
    m_currentSettings = settings;

    SampleInfo ex;

    updateValues(ex, m_currentSettings);

    m_first->display(ex.fVal);
    m_signLbl->setText(QString(ex.sign));
    m_second->display(ex.sVal);
}

void SampleWidget::checkResult()
{
    CommonExampleWidget::checkResult();
}

void SampleWidget::keyPressEvent(QKeyEvent *e)
{
    if(e->key() >= Qt::Key_0 && e->key() <= Qt::Key_9){
        return;

    }

    CommonExampleWidget::keyPressEvent(e);
}
