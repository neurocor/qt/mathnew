#ifndef SETTINGS_H
#define SETTINGS_H

#include <QList>

struct Settings{

    int minVal{0};
    int maxVal{0};
    int resVal{0};

    bool inequality{false};
    bool column{false};

    QList <char> signs;
    QList <char> inequalitySigns{'<', '>', '='};

};

#endif // SETTINGS_H
