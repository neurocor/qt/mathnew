#ifndef EXAMPLEWIDGET_H
#define EXAMPLEWIDGET_H

#include <QStackedWidget>

#include "settings.h"

class InequalityWidget;

class ExampleWidget : public QStackedWidget
{
    Q_OBJECT
public:
    explicit ExampleWidget(QWidget *parent = nullptr);

    void updateSettings(const Settings &settings);
    void generate();

    void check();


private:

    InequalityWidget *m_ineqW;

    Settings m_currentSettings;
};

#endif // EXAMPLEWIDGET_H
