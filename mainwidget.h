#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>

struct Settings;

class ExampleWidget;

class MainWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MainWidget(QWidget *parent = nullptr);

    void setSettings(const Settings &settings);


private:

    ExampleWidget *m_exW;
};

#endif // MAINWIDGET_H
