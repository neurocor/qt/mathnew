#include "settingswidget.h"

#include <QFormLayout>
#include <QPushButton>
#include <QCheckBox>
#include <QSpinBox>

#include "settings.h"

SettingsWidget::SettingsWidget(QWidget *parent)
    : QWidget{parent}
{
    auto mainLay = new QFormLayout(this);

    m_minValSB = new QSpinBox(this);
    m_maxValSB = new QSpinBox(this);
    m_resValSB = new QSpinBox(this);

    auto closeBtn = new QPushButton("Close", this);
    auto acceptBtn = new QPushButton("Accept", this);
    auto inequalityChB = new QCheckBox("Inequality", this);

    m_plusChB = new QCheckBox("+", this);
    m_minusChB = new QCheckBox("-", this);
    m_mulChB = new QCheckBox("*", this);
    m_devChB = new QCheckBox(":", this);

    m_columnChB = new QCheckBox("Column", this);

    m_columnChB->setEnabled(false);

    mainLay->addRow("Minimum value", m_minValSB);
    mainLay->addRow("Maximum value", m_maxValSB);
    mainLay->addRow("Result value", m_resValSB);

    mainLay->addRow(m_plusChB, m_minusChB);
    mainLay->addRow(m_mulChB, m_devChB);
    mainLay->addRow(inequalityChB);
    mainLay->addRow(m_columnChB);

    mainLay->addRow(closeBtn, acceptBtn);


    //    connect(acceptBtn,&QPushButton::clicked);

    connect(acceptBtn,&QPushButton::clicked,[this, inequalityChB]{

        Settings st;

        st.minVal = m_minValSB->value();
        st.maxVal = m_maxValSB->value();
        st.resVal = m_resValSB->value();

        if(m_plusChB->isChecked())
            st.signs.append('+');
        if(m_minusChB->isChecked())
            st.signs.append('-');
        if(m_mulChB->isChecked())
            st.signs.append('*');
        if(m_devChB->isChecked())
            st.signs.append(':');


        st.inequality = inequalityChB->isChecked();

        emit accepted(st);
    });


}
