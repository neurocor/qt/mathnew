#ifndef COLUMNWIDGET_H
#define COLUMNWIDGET_H

#include "commonexamplewidget.h"

class ColumnWidget : public CommonExampleWidget
{
    Q_OBJECT
public:
    explicit ColumnWidget(QWidget *parent = nullptr);
};

#endif // COLUMNWIDGET_H
