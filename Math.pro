QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    columnwidget.cpp \
    commonexamplewidget.cpp \
    examplewidget.cpp \
    inequalitywidget.cpp \
    main.cpp \
    mainwidget.cpp \
    resultwidget.cpp \
    samplewidget.cpp \
    settingswidget.cpp \
    widget.cpp

HEADERS += \
    columnwidget.h \
    commonexamplewidget.h \
    exampleinfo.h \
    examplewidget.h \
    inequalityinfo.h \
    inequalitywidget.h \
    mainwidget.h \
    result.h \
    resultwidget.h \
    sampleinfo.h \
    samplewidget.h \
    settings.h \
    settingswidget.h \
    widget.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
