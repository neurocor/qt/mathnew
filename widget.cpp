#include "widget.h"

#include "mainwidget.h"
#include "settingswidget.h"


Widget::Widget(QWidget *parent)
    : QStackedWidget(parent)
{
    auto settingsW = new SettingsWidget(this);
    auto mainW = new MainWidget(this);

    addWidget(settingsW);
    addWidget(mainW);

    connect(settingsW,&SettingsWidget::accepted,[this, mainW](const Settings &st){
        setWindowState(Qt::WindowState::WindowMaximized);
        mainW->setSettings(st);
        setCurrentWidget(mainW);
    });
}

Widget::~Widget()
{
}

