#include "inequalitywidget.h"

#include <QKeyEvent>

#include <QLayout>
#include <QLabel>
#include <QLCDNumber>

#include "settings.h"


InequalityWidget::InequalityWidget(QWidget *parent)
    : CommonExampleWidget{parent}
{
    auto mainLay=new QHBoxLayout(this);

    m_inequalities.clear();

    m_lFirst= new QLCDNumber(this);
    m_lSecond= new QLCDNumber(this);
    m_rFirst= new QLCDNumber(this);
    m_rSecond= new QLCDNumber(this);

    m_ineqSignLbl=new QLabel(this);
    m_lSignLbl=new QLabel(this);
    m_rSignLbl=new QLabel(this);

    mainLay->addWidget(m_lFirst);
    mainLay->addWidget(m_lSignLbl);
    mainLay->addWidget(m_lSecond);
    mainLay->addWidget(m_ineqSignLbl);
    mainLay->addWidget(m_rFirst);
    mainLay->addWidget(m_rSignLbl);
    mainLay->addWidget(m_rSecond);


    setupLCD(m_lFirst);
    setupLCD(m_lSecond);
    setupLCD(m_rFirst);
    setupLCD(m_rSecond);


    setupLbl(m_ineqSignLbl);
    setupLbl(m_lSignLbl);
    setupLbl(m_rSignLbl);

}

void InequalityWidget::genarateValues(const Settings &settings)
{
    m_currentSettings = settings;

    auto lExample = curInequality.lExample;
    auto rExample = curInequality.rExample;
    auto resSign= curInequality.resSign;

    updateValues(lExample, m_currentSettings);
    updateValues(rExample, m_currentSettings);

    if(lExample.rVal < rExample.rVal)
        resSign='<';
    else if(lExample.rVal == rExample.rVal)
        resSign='=';
    else
        resSign='>';


    m_lFirst->display(lExample.fVal);
    m_lSignLbl->setText(QString(lExample.sign));
    m_lSecond->display(lExample.sVal);

    m_rFirst->display(rExample.fVal);
    m_rSignLbl->setText(QString(rExample.sign));
    m_rSecond->display(rExample.sVal);

    m_ineqSignLbl->clear();

    curInequality.lExample = lExample;
    curInequality.rExample = rExample;
    curInequality.resSign = resSign;

}

QList<InequalityInfo> InequalityWidget::result() const
{
    return m_inequalities;
}

void InequalityWidget::checkResult()
{
    if(m_ineqSignLbl->text().isEmpty())
        return;

    curInequality.ansSign = *static_cast<char*>(m_ineqSignLbl->text().toLocal8Bit().data());

    curInequality.isRight =  curInequality.ansSign ==  curInequality.resSign;

    m_inequalities.append(curInequality);

    CommonExampleWidget::checkResult();
}

void InequalityWidget::keyPressEvent(QKeyEvent *e)
{
    if(e->key() >= Qt::Key_Less && e->key() <= Qt::Key_Greater){
        m_ineqSignLbl->setText(e->text());
    }

    CommonExampleWidget::keyPressEvent(e);
}
