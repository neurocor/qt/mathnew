#ifndef SETTINGSWIDGET_H
#define SETTINGSWIDGET_H

#include <QWidget>

struct Settings;
struct QCheckBox;
struct QSpinBox;

class SettingsWidget : public QWidget
{
    Q_OBJECT
public:
    explicit SettingsWidget(QWidget *parent = nullptr);

signals:

    void accepted(const Settings &settings);

private:

    QSpinBox *m_minValSB;
    QSpinBox *m_maxValSB;
    QSpinBox *m_resValSB;

    QCheckBox *m_plusChB;
    QCheckBox *m_minusChB;
    QCheckBox *m_devChB;
    QCheckBox *m_mulChB;

    QCheckBox *m_columnChB;

};

#endif // SETTINGSWIDGET_H
